package cl.coopeuch.mserv.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "persona")
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Persona {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpersona")
    private Long 	id;

    @Column(name = "firstname")
    private String	firstName;

    @Column(name = "lastname")
    private String	lastName;

    @Column(name = "status")
    private Boolean	status;

    @Column(name = "creation")
    private Date	creation;

}