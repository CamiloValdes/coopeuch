package cl.coopeuch.mserv.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "tarea")
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class Tarea {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idtarea")
    private Long 	id;

    @Column(name = "descripcion")
    private String	descripcion;

    @Column(name = "status")
    private Boolean	status;

    @Column(name = "creation")
    private Date	creation;
    
    @ManyToOne
    @JoinColumn(name = "idpersona", referencedColumnName = "idpersona")
    private Persona persona;

}