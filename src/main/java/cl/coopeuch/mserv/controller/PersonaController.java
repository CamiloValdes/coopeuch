package cl.coopeuch.mserv.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.coopeuch.mserv.service.PersonaService;
import cl.coopeuch.mserv.vo.PersonaVO;
import io.swagger.annotations.Api;

@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("/v1/coopeuch")
@Api(tags={"Datos Persona"})
public class PersonaController {
	
	@Autowired
	private PersonaService personaService;
	
	/*******************************************************************************************/
		
	@GetMapping(value = "/persona")
	@CrossOrigin(origins = "*", methods = {RequestMethod.GET})
    public ResponseEntity<List<PersonaVO>> fetchAll(){
		return new ResponseEntity<>(this.personaService.fetchAllPersona(), HttpStatus.OK);
    }
}