package cl.coopeuch.mserv.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import cl.coopeuch.mserv.entity.Tarea;
import cl.coopeuch.mserv.service.TareaService;
import cl.coopeuch.mserv.vo.TareaRequest;
import cl.coopeuch.mserv.vo.TareaVO;
import io.swagger.annotations.Api;

@Validated
@RestController
@EnableAutoConfiguration
@RequestMapping("/v1/coopeuch")
@Api(tags={"Datos Tareas-Persona"})
public class TareaController {

	@Autowired
	private TareaService tareaService;
	
	/*******************************************************************************************/
	
	@GetMapping(value = "/tarea")
    @CrossOrigin(origins = "*", methods = {RequestMethod.GET})
    public ResponseEntity<List<TareaVO>> fetchId(){
		return new ResponseEntity<>(this.tareaService.fetchTareas(), HttpStatus.OK);
    }
	
	@GetMapping(value = "/tarea/{idPersona}")
    @CrossOrigin(origins = "*", methods = {RequestMethod.GET})
    public ResponseEntity<List<TareaVO>> fetchIdPersona(@PathVariable("idPersona") Long idPersona){
		return new ResponseEntity<>(this.tareaService.fetchTareasPersona(idPersona), HttpStatus.OK);
    }
	
	@PostMapping(value = "/tarea")
    @CrossOrigin(origins = "*", methods = {RequestMethod.POST})
    public ResponseEntity<Tarea> saveTarea(@Valid @RequestBody TareaRequest req){
		return new ResponseEntity<>(this.tareaService.saveTarea(req), HttpStatus.OK);
    }
	
	@PutMapping(value = "/tarea")
    @CrossOrigin(origins = "*", methods = {RequestMethod.PUT})
    public ResponseEntity<Tarea> updateTarea(@Valid @RequestBody TareaRequest req){
		return new ResponseEntity<>(this.tareaService.updateTarea(req), HttpStatus.OK);
    }
	
	@DeleteMapping(value = "/tarea/{id}")
    @CrossOrigin(origins = "*", methods = {RequestMethod.DELETE})
    public ResponseEntity<Boolean> deleteTarea(@PathVariable("id") Long id){
		return new ResponseEntity<>(this.tareaService.deleteTarea(id), HttpStatus.OK);
    }
	
}
