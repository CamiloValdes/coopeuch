package cl.coopeuch.mserv.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import cl.coopeuch.mserv.entity.Tarea;

@Repository
public interface TareaRepository extends JpaRepository<Tarea, Long> {

	@Query("SELECT t FROM Tarea t WHERE t.persona.id=:idPersona AND t.persona.status=TRUE")
	public List<Tarea> findByTareaPersona(Long idPersona);

}