package cl.coopeuch.mserv.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import cl.coopeuch.mserv.entity.Persona;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long> {

}