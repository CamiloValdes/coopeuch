package cl.coopeuch.mserv.vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PersonaVO {
	private Long 	id;
    private String	nombre;
    private String	apellido;
    private Boolean	estado;
    private String	fechaCreacion;

    private List<TareaVO> tareasList;

    @AllArgsConstructor
    @Data
    public static class TareaVO {
    	
    	private String	descripcion;
    	
    }

}