package cl.coopeuch.mserv.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TareaVO {

	private Long 	id;
	private Long 	idPersona;
	private	String	nombrePersona;
	private Boolean	estadoPersona;
    private String	descripcion;
    private Boolean	estadoTarea;
    private String	fechaCreacion;
	
}
