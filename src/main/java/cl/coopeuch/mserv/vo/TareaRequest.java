package cl.coopeuch.mserv.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class TareaRequest {

	private	Long	idTarea;
	private Long 	idPersona;
	private String	descripcion;
	private Boolean	status;

}