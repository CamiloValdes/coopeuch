package cl.coopeuch.mserv.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.coopeuch.mserv.entity.Persona;
import cl.coopeuch.mserv.entity.Tarea;
import cl.coopeuch.mserv.repository.PersonaRepository;
import cl.coopeuch.mserv.repository.TareaRepository;
import cl.coopeuch.mserv.utils.UtilsText;
import cl.coopeuch.mserv.vo.TareaRequest;
import cl.coopeuch.mserv.vo.TareaVO;

@Service
public class TareaService {

	@Autowired
	private TareaRepository tareaRepository;
	
	@Autowired
	private PersonaRepository personaRepository;

	@Autowired
	private UtilsText utilsText;

	/*******************************************************************************************/
	
	public List<TareaVO> fetchTareas() {
		return
		this.tareaRepository.findAll().stream().map(t -> {
			return new TareaVO(
				t.getId(), t.getPersona().getId(), this.utilsText.concatString(t.getPersona().getFirstName(), t.getPersona().getLastName()),
				t.getPersona().getStatus(), t.getDescripcion(), t.getStatus(), this.utilsText.formatDate(t.getCreation())
			);
		}).collect(Collectors.toList());
	}
	
	public List<TareaVO> fetchTareasPersona(Long idPersona) {
		return
		this.tareaRepository.findByTareaPersona(idPersona).stream().map(t -> {
			return new TareaVO(
				t.getId(), t.getPersona().getId(), this.utilsText.concatString(t.getPersona().getFirstName(), t.getPersona().getLastName()),
				t.getPersona().getStatus(), t.getDescripcion(), t.getStatus(), this.utilsText.formatDate(t.getCreation())
			);
		}).collect(Collectors.toList());
	}
	
	public Tarea saveTarea(TareaRequest tarea) {
		Optional<Persona> optPersona = this.personaRepository.findById(tarea.getIdPersona());
		if(optPersona.isPresent()) {
			return this.tareaRepository.save(
				new Tarea(
					null,
					tarea.getDescripcion(),
					Boolean.TRUE,
					this.utilsText.now(),
					optPersona.get()
				)
			);
		}else {
			return null;
		}
	}
	
	public Tarea updateTarea(TareaRequest tarea) {
		Optional<Tarea> optTarea = this.tareaRepository.findById(tarea.getIdTarea());
		if(optTarea.isPresent()) {
			Optional<Persona> optPersona = this.personaRepository.findById(tarea.getIdPersona());
			if(optPersona.isPresent()) {
				return this.tareaRepository.save(
					new Tarea(
						optTarea.get().getId(),
						tarea.getDescripcion(),
						tarea.getStatus(),
						this.utilsText.now(),
						optPersona.get()
					)
				);
			}
			return null;
		}else {
			return null;
		}
	}
	
	public Boolean deleteTarea(Long id) {
		Optional<Tarea> optTarea = this.tareaRepository.findById(id);
		if(optTarea.isPresent()) {
			this.tareaRepository.delete(optTarea.get());
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}
	
}
