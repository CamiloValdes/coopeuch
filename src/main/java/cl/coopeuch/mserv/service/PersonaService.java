package cl.coopeuch.mserv.service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.coopeuch.mserv.repository.PersonaRepository;
import cl.coopeuch.mserv.repository.TareaRepository;
import cl.coopeuch.mserv.utils.UtilsText;
import cl.coopeuch.mserv.vo.PersonaVO;

@Service
public class PersonaService {

	@Autowired
	private PersonaRepository personaRepository;
	
	@Autowired
	private TareaRepository tareaRepository;
	
	@Autowired
	private UtilsText utilsText;
	
	/*******************************************************************************************/
	
	public List<PersonaVO> fetchAllPersona() {
		try {
			return
			this.personaRepository.findAll()
			.stream()
			.map(p -> {
				return new PersonaVO(
					p.getId(),
					p.getFirstName(),
					p.getLastName(),
					p.getStatus(),
					this.utilsText.formatDate(p.getCreation()),
					this.fetchTareasPersona(p.getId())
				);
			}).collect(Collectors.toList());
		}catch(Exception ex) {
			return Collections.emptyList();
		}
	}

	/*******************************************************************************************/

	private	List<PersonaVO.TareaVO> fetchTareasPersona(Long idPersona) {
		return
		this.tareaRepository.findByTareaPersona(idPersona).stream().map(m -> {
			return new PersonaVO.TareaVO(m.getDescripcion());
		}).collect(Collectors.toList());
	}

	/*******************************************************************************************/

}