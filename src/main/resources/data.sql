DROP TABLE IF EXISTS persona;

CREATE TABLE persona (
  idpersona bigint        AUTO_INCREMENT PRIMARY KEY,
  firstname varchar(250)  NOT NULL,
  lastname  varchar(250)  NULL,
  status    boolean       NOT NULL,
  creation  date          NOT NULL
);

INSERT INTO persona (idpersona, firstname, lastname, status, creation) VALUES
  (1, 'Camilo' , 'Valdés' , 1, CURRENT_TIMESTAMP),
  (2, 'Antonia', 'Valdés' , 0, CURRENT_TIMESTAMP),
  (3, 'Gabriel', 'Valdés' , 1, CURRENT_TIMESTAMP);


DROP TABLE IF EXISTS tarea;

CREATE TABLE tarea (
  idtarea     bigint        AUTO_INCREMENT PRIMARY KEY,
  idpersona   bigint        NOT NULL,
  descripcion varchar(250)  NOT NULL,
  status      boolean       NOT NULL,
  creation    date          NOT NULL
);

INSERT INTO tarea (idpersona, descripcion, status, creation) VALUES
  (1, 'Tarea de Requerimiento 001', 1, CURRENT_TIMESTAMP),
  (1, 'Tarea de Requerimiento 002', 1, CURRENT_TIMESTAMP),
  (2, 'Tarea de Requerimiento 003', 1, CURRENT_TIMESTAMP),
  (3, 'Tarea de Requerimiento 004', 1, CURRENT_TIMESTAMP);